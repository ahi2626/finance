package source.it.interfaces;


public interface Deposit {
    float depositing(int money);
}
