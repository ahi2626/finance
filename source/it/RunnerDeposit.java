package source.it;

import source.it.data.Generator;
 import source.it.interfaces.Deposit;
import source.it.model.Organization;

import java.util.List;
import java.util.Scanner;


public class RunnerDeposit {
    public static void runTask() {

        List<Organization> organizations = Generator.generate ();
        Scanner scan = new Scanner ( System.in );

        System.out.println ( "Какая сумму денег вы собираетесь положить на депозит: " );
        int inputMoney = Integer.parseInt ( scan.nextLine () );


        float highRate = Float.MIN_VALUE;
        int check = 0;
        for ( int i = 0; i<organizations.size(); i++ ) {
            if(organizations.get(i) instanceof Deposit) {
                Deposit deposit = (Deposit) organizations.get(i);
                float checkSum = deposit.depositing ( inputMoney );
                if(checkSum > highRate ) {
                    highRate = checkSum;
                    check = i;
                }
            }
        }
        System.out.println ( "Лучшей организацией для размещения депозита является: " );
        organizations.get ( check ).showInfo ();
        System.out.println (String.format ( "Сумма депозита через год будут составлять = %2f",highRate ));

    }
}