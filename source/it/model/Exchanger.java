package source.it.model;

import source.it.interfaces.ExchangeCurrency;

import java.util.Map;

public class Exchanger extends Organization implements ExchangeCurrency {
    private Map< String, Currency > rate;
    private static String ACTION = "купить-продать валюту";


    public Exchanger(String name, String address, Map< String, Currency > rate) {
        super ( name, address );
        this.rate = rate;

    }

    @Override
    public float currencyOfBuying(int money, String inputNameOfCurrency) {
        Currency currency = rate.get ( inputNameOfCurrency.toLowerCase () );
        if (currency == null) {
            return 0;
        } else {
            return money / currency.getСurrencyOfBuying ();
        }
    }

    @Override
    public float currencyOfSelling(int money, String inputNameOfCurrency) {
        Currency currency = rate.get ( inputNameOfCurrency.toLowerCase () );
        if (currency == null) {
            return 0;
        } else {
            return (money * currency.getСurrencyOfSelling ());
        }
    }


    @Override
    public boolean BuySell(String inputChooseCurrency) {
        return ACTION.toLowerCase ().contains ( inputChooseCurrency.toLowerCase () );
    }


    @Override
    public void showInfo() {
        System.out.println ( String.format ( "Название: %s | Адрес: %s ", super.name, super.address ) );
    }
}
