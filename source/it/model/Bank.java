package source.it.model;


import source.it.interfaces.Credit;
import source.it.interfaces.Deposit;
import source.it.interfaces.ExchangeCurrency;
import source.it.interfaces.Transfer;

import java.util.Map;

public class Bank extends Organization implements ExchangeCurrency, Credit, Deposit, Transfer {
    protected int yearOfLicence;
    private Map< String, Currency > rate;
    private int commissionOfExchange = 15;
    private int maxLimitOfExchange = 150000;
    private int debtLimit = 200000;
    private float creditPercent = 0.25f;
    private float percentOfDeposit = 0.1f;
    private float percentOfTransfer = 0.1f;
    private static String ACTION = "купить-продать валюту";


    public Bank(String name, String address, int yearOfLicence, Map< String, Currency > rate) {
        super ( name, address );
        this.yearOfLicence = yearOfLicence;
        this.rate = rate;

    }


    @Override
    public float currencyOfBuying(int money, String inputNameOfCurrency) {
        Currency curr = rate.get ( inputNameOfCurrency.toLowerCase () );
        if (hasMoreIfLimit ( money, maxLimitOfExchange )) {
            return (money - commissionOfExchange) / curr.getСurrencyOfBuying ();
        }
        return 0;
    }


    @Override
    public float currencyOfSelling(int money, String inputNameOfCurrency) {
        Currency curr = rate.get ( inputNameOfCurrency.toLowerCase () );
        float cashFlow = money * curr.getСurrencyOfSelling ();
        if (hasMoreIfLimit ( cashFlow, maxLimitOfExchange )) {
            return (cashFlow - commissionOfExchange);
        }
        return 0;
    }


    public boolean hasMoreIfLimit(float cashFlow, int limit) {
        return cashFlow < limit;
    }

    @Override
    public void showInfo() {
        System.out.println ( String.format ( "Название: %s , Адрес: %s ,Год получения лицензии: %d ", super.name, super.address, yearOfLicence ) );
        System.out.println ( "Банк предоставляет услуги: " );
        System.out.println ( String.format ( "Курс обмена гривну на USD, EUR и RUB и наоборот, с минимальной комиссией = %d гривен", commissionOfExchange ) );
        System.out.println ( String.format ( "Выдача кредитов на сумму не более %d гривен, под приятный процент годовых = %.0f", debtLimit, creditPercent * 100 ) );
        System.out.println ( String.format ( "Оформление  депозита на год с хорошим процентом = %.0f", percentOfDeposit * 100 ) );
        System.out.println ( String.format ( "Перевод средств с банковским процентом = %.0f", percentOfTransfer * 100 ) );
    }


    @Override
    public boolean BuySell(String inputChooseCurrency) {
        return ACTION.toLowerCase ().contains ( inputChooseCurrency.toLowerCase () );
    }


    @Override
    public float crediting(int money) {
        if (hasMoreIfLimit ( money, debtLimit )) {
            return money * (1 + creditPercent);
        }
        return 0;
    }

    @Override
    public float depositing(int money) {
        return money * (1 + percentOfDeposit);
    }


    @Override
    public float transfering(int money) {
        return money * (1 + percentOfTransfer);
    }
}





