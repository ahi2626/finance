package source.it.model;


import source.it.interfaces.Transfer;


public class Transfering extends Organization implements Transfer {
    private float percentOfTransfer;


    public Transfering(String name, String address, float percentOfTransfer) {
        super ( name, address );
        this.percentOfTransfer = percentOfTransfer;
    }


    @Override
    public float transfering(int money) {
       return money * (1 + percentOfTransfer);
    }

    @Override
    public void showInfo() {
        System.out.println ( String.format ( "Название: %s | Адрес: %s |Процент перевода: %f", super.name, super.address, percentOfTransfer ) );
    }

}