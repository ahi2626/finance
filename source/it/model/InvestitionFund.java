package source.it.model;

import source.it.interfaces.Deposit;

public class InvestitionFund extends Organization implements Deposit {
   private int yearOfFoundation;
    private float percentOfDeposit;
    
    public InvestitionFund(String name, String address, int yearOfFoundation, float percentOfDeposit){
        super(name,address);
        this.yearOfFoundation = yearOfFoundation;
        this.percentOfDeposit = percentOfDeposit;
    }



    @Override
    public float depositing(int money) {

        return money  * (1 + percentOfDeposit);
    }

    @Override
    public void showInfo() {
        System.out.println (String.format ("Название: %s | Адрес: %s |Год основания: %d |Процент депозита на год: %.0f", super.name, super.address, yearOfFoundation, percentOfDeposit * 100));
    }
}
