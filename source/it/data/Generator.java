package source.it.data;


import source.it.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Generator {
    private Generator() {
    }


    public static List< Organization > generate() {
        List< Organization > organizations = new ArrayList<> ();
        {

            Map< String, Currency > rate = new HashMap<> ();
            rate.put ( "usd", new Currency ( 27f, 25f ) );
            rate.put ( "eur", new Currency ( 30f, 32f ) );
            rate.put ( "rub", new Currency ( 0.35f, 0.4f ) );
            organizations.add ( 0, new Bank ( "ПриватБанк", "Сумская 1", 1996, rate ) );
        }

        {
            Map< String, Currency > rate = new HashMap<> ();
            rate.put ( "usd", new Currency ( 25f, 24f ) );
            rate.put ( "eur", new Currency ( 29f, 27f ) );
            organizations.add ( new Exchanger ( "Обменочка №1", "Сумская 11", rate ) );
        }
        organizations.add ( new CreditOrganization ( "Ломбард Скупщик", "Сумская 150", 50000, 0.4f ) );

        organizations.add ( new CreditOrganization ( "Кредит Кафе Орелнок", "Сумская 15", 4000, 2f ) );

        organizations.add ( new CreditOrganization ( "Кредит Союз Титан", "Сумская 45", 20000, 0.2f ) );

        organizations.add ( new InvestitionFund ( "Петрушка", "Сумская 88", 2010, 0.15f ) );

        organizations.add ( new Transfering ( "УкрПочта", "Сумская 277", 0.02f ) );

        organizations.add ( new Transfering ( "Transfer Money", "Сумская 77", 0.1f ) );


        return organizations;

    }

}

