package source.it;

import source.it.data.Generator;
import source.it.interfaces.Transfer;
import source.it.model.Organization;

import java.util.List;
import java.util.Scanner;



public class RunnerTransfer {
    public static  void runTask() {

        List<Organization> organizations = Generator.generate ();
        Scanner scan = new Scanner ( System.in );

        System.out.println ( "Какая сумму вы хотите перевести: " );
        int inputMoney = Integer.parseInt ( scan.nextLine () );

        float lowRate = Float.MAX_VALUE;
        int check = 0;
        for ( int i = 0; i<organizations.size(); i++ ) {
            if(organizations.get(i) instanceof Transfer) {
                Transfer transfer = (Transfer) organizations.get(i);
                float checkSum = transfer.transfering ( inputMoney );
                if(checkSum < lowRate ) {
                    lowRate = checkSum;
                    check = i;
                }
            }
        }



        System.out.println ( "Лучшей организацией для перевода является: " );
        organizations.get ( check ).showInfo ();
        System.out.println (String.format ( "Для перевода суммы: %d затраты вместе с комиссией будут равны %.2f",inputMoney,lowRate ));
    }

}