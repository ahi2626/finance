package source.it;

import source.it.data.Generator;
import source.it.interfaces.ExchangeCurrency;
import source.it.model.Organization;

import java.util.List;
import java.util.Scanner;


public class RunnerExchange {


    public void runTask() {

        List< Organization > organizations = Generator.generate ();
        Scanner scan = new Scanner ( System.in );

        System.out.println ( "Какую валюту хотите обменять ( usd eur rub ):?" );
        String inputNameOfCurrency = scan.nextLine ();

        System.out.println ( "Желаете купить или продать валюту:?" );
        String inputChooseCurrency = scan.nextLine ();

        System.out.println ( "Введите нужную вам сумму:?" );
        int inputMoney = Integer.parseInt ( scan.nextLine () );


        float variableFirst;
        if (inputMoney > 0) {
            for ( Organization organization : organizations ) {
                if (organization instanceof ExchangeCurrency) {
                    ExchangeCurrency ExcCur = (ExchangeCurrency) organization;
                    if (ExcCur.BuySell ( inputChooseCurrency )) {
                        variableFirst = ExcCur.currencyOfSelling ( inputMoney, inputNameOfCurrency );
                    } else {
                        variableFirst = ExcCur.currencyOfBuying ( inputMoney, inputNameOfCurrency );
                    }
                    if (variableFirst != 0) {
                        organization.showInfo ();
                        System.out.println ( String.format ( "Где результат вашей операции: %.2f", variableFirst ) );
                    } else {
                        System.out.print ( "В данной организации операция невозможна: " );
                        organization.showInfo ();
                    }
                }
            }
        } else {
                System.out.println("Операция невозможна");
            }
        }
    }


//  float variableFirst = 0f;
            //        float checkFirst = 0f;
            //        int check = 0;
            //
            //
            //
            //        for ( int i = 0; i < organizations.size(); i++ ) {
            //            if (organizations.get(i) instanceof ExchangeCurrency) {
            //                ExchangeCurrency ExcCur = (ExchangeCurrency) organizations.get(i);
            //                if (ExcCur.BuySell ( inputChooseCurrency )) {
            //                    checkFirst = ExcCur.currencyOfSelling ( inputMoney, inputNameOfCurrency );
            //                } else {
            //                    checkFirst = ExcCur.currencyOfBuying ( inputMoney, inputNameOfCurrency );
            //                }
            //
            //
            //
            //                if (checkFirst > variableFirst) {
            //                    variableFirst = checkFirst;
            //                    check = i;
            //                }
            // }
            // }
            //        System.out.println ( "По вашему запросу лучшая организация для проведения операции это: " );
            //        organizations.get ( check ).showInfo ();
            //        System.out.println ( String.format ( "Где результат вашей операции: %.2f", variableFirst ) );
