package source.it;

import source.it.data.Generator;
import source.it.interfaces.Credit;
import source.it.model.Organization;

import java.util.List;
import java.util.Scanner;



public class RunnerCredit {
    public static void runTask() {

        List<Organization> organizations = Generator.generate ();
        Scanner scan = new Scanner ( System.in );

        System.out.println ( "Какая сумму денег которая вам необходима: " );
        int inputMoney = Integer.parseInt ( scan.nextLine () );

        float lowRate = Float.MAX_VALUE;
        int check = 0;
        for ( int i = 0; i<organizations.size(); i++ ) {
            if(organizations.get(i) instanceof Credit) {
                 Credit credit = (Credit) organizations.get(i);
                 float checkSum = credit.crediting ( inputMoney );
                   if(checkSum < lowRate && checkSum !=0) {
                     lowRate = checkSum;
                     check = i;
                 }
            }
        }



        System.out.println ( "Лучшей организацией для получения кредита является: " );
        organizations.get ( check ).showInfo ();
        System.out.println (String.format ( "Выплаты по кредиту на сумму %d через год будут составлять = %2f",inputMoney,lowRate ));
    }

}
